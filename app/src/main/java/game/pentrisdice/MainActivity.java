package game.pentrisdice;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MainActivity extends AppCompatActivity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;

    private Button mBtnThrow ;
    private TextView mTvDice ;
    private int iDiceValue = 0 ;
    private static final int MAX_THROW_WITH_SAME_VALUE = 2 ;
    private int iCountWithSameDiceValue = 0 ;

    private int[] mPentaminoLeft ;
    private int[] mPentaminoRight ;

    private int[] mPentaminoNoShape = {
            -1, -1, -1,
            -1, -1, -1,
            -1, -1, -1,
            -1, -1, -1,
            -1, -1, -1 };

    private int[] mPentamino1Left = {
            0, 1, 1,
            1, 1, 0,
            0, 1, 0,
            0, 0, 0,
            0, 0, 0 };

    private int[] mPentamino1Right = {
            0, 1, 0,
            0, 1, 0,
            0, 1, 0,
            0, 1, 0,
            0, 1, 0 };

    private int[] mPentamino2Left = {
            2, 0, 0,
            2, 0, 0,
            2, 0, 0,
            2, 2, 0,
            0, 0, 0 };

    private int[] mPentamino2Right = {
            0, 0, 2,
            0, 2, 2,
            0, 2, 0,
            0, 2, 0,
            0, 0, 0 };

    private int[] mPentamino3Left = {
            3, 3, 0,
            3, 3, 0,
            3, 0, 0,
            0, 0, 0,
            0, 0, 0 };

    private int[] mPentamino3Right = {
            3, 3, 3,
            0, 3, 0,
            0, 3, 0,
            0, 0, 0,
            0, 0, 0 };

    private int[] mPentamino4Left = {
            0, 0, 0,
            0, 0, 0,
            4, 0, 4,
            4, 4, 4,
            0, 0, 0 };

    private int[] mPentamino4Right = {
            0, 0, 0,
            4, 0, 0,
            4, 0, 0,
            4, 4, 4,
            0, 0, 0 };

    private int[] mPentamino5Left = {
            0, 0, 0,
            5, 0, 0,
            5, 5, 0,
            0, 5, 5,
            0, 0, 0 };

    private int[] mPentamino5Right = {
            0, 0, 0,
            0, 5, 0,
            5, 5, 5,
            0, 5, 0,
            0, 0, 0 };

    private int[] mPentamino6Left = {
            0, 5, 0,
            5, 5, 0,
            0, 5, 0,
            0, 5, 0,
            0, 0, 0 };

    private int[] mPentamino6Right = {
            0, 0, 0,
            5, 5, 0,
            0, 5, 0,
            0, 5, 5,
            0, 0, 0 };

    private ImageView m_ivPentaminoLeftR1C1, m_ivPentaminoRightR1C1 ;
    private ImageView m_ivPentaminoLeftR1C2, m_ivPentaminoRightR1C2 ;
    private ImageView m_ivPentaminoLeftR1C3, m_ivPentaminoRightR1C3 ;
    private ImageView m_ivPentaminoLeftR2C1, m_ivPentaminoRightR2C1 ;
    private ImageView m_ivPentaminoLeftR2C2, m_ivPentaminoRightR2C2 ;
    private ImageView m_ivPentaminoLeftR2C3, m_ivPentaminoRightR2C3 ;
    private ImageView m_ivPentaminoLeftR3C1, m_ivPentaminoRightR3C1 ;
    private ImageView m_ivPentaminoLeftR3C2, m_ivPentaminoRightR3C2 ;
    private ImageView m_ivPentaminoLeftR3C3, m_ivPentaminoRightR3C3 ;
    private ImageView m_ivPentaminoLeftR4C1, m_ivPentaminoRightR4C1 ;
    private ImageView m_ivPentaminoLeftR4C2, m_ivPentaminoRightR4C2 ;
    private ImageView m_ivPentaminoLeftR4C3, m_ivPentaminoRightR4C3 ;
    private ImageView m_ivPentaminoLeftR5C1, m_ivPentaminoRightR5C1 ;
    private ImageView m_ivPentaminoLeftR5C2, m_ivPentaminoRightR5C2 ;
    private ImageView m_ivPentaminoLeftR5C3, m_ivPentaminoRightR5C3 ;

    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            //mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (AUTO_HIDE) {
                        delayedHide(AUTO_HIDE_DELAY_MILLIS);
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    view.performClick();
                    break;
                default:
                    break;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mVisible = true;
        //mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = findViewById(R.id.content_view);

        mBtnThrow = findViewById(R.id.btnThrow);
        mTvDice = findViewById(R.id.tvDice);

        m_ivPentaminoLeftR1C1 = findViewById( R.id.ivPentaminoLeftR1C1) ; m_ivPentaminoRightR1C1 = findViewById( R.id.ivPentaminoRightR1C1) ;
        m_ivPentaminoLeftR1C2 = findViewById( R.id.ivPentaminoLeftR1C2) ; m_ivPentaminoRightR1C2 = findViewById( R.id.ivPentaminoRightR1C2) ;
        m_ivPentaminoLeftR1C3 = findViewById( R.id.ivPentaminoLeftR1C3) ; m_ivPentaminoRightR1C3 = findViewById( R.id.ivPentaminoRightR1C3) ;
        m_ivPentaminoLeftR2C1 = findViewById( R.id.ivPentaminoLeftR2C1) ; m_ivPentaminoRightR2C1 = findViewById( R.id.ivPentaminoRightR2C1) ;
        m_ivPentaminoLeftR2C2 = findViewById( R.id.ivPentaminoLeftR2C2) ; m_ivPentaminoRightR2C2 = findViewById( R.id.ivPentaminoRightR2C2) ;
        m_ivPentaminoLeftR2C3 = findViewById( R.id.ivPentaminoLeftR2C3) ; m_ivPentaminoRightR2C3 = findViewById( R.id.ivPentaminoRightR2C3) ;
        m_ivPentaminoLeftR3C1 = findViewById( R.id.ivPentaminoLeftR3C1) ; m_ivPentaminoRightR3C1 = findViewById( R.id.ivPentaminoRightR3C1) ;
        m_ivPentaminoLeftR3C2 = findViewById( R.id.ivPentaminoLeftR3C2) ; m_ivPentaminoRightR3C2 = findViewById( R.id.ivPentaminoRightR3C2) ;
        m_ivPentaminoLeftR3C3 = findViewById( R.id.ivPentaminoLeftR3C3) ; m_ivPentaminoRightR3C3 = findViewById( R.id.ivPentaminoRightR3C3) ;
        m_ivPentaminoLeftR4C1 = findViewById( R.id.ivPentaminoLeftR4C1) ; m_ivPentaminoRightR4C1 = findViewById( R.id.ivPentaminoRightR4C1) ;
        m_ivPentaminoLeftR4C2 = findViewById( R.id.ivPentaminoLeftR4C2) ; m_ivPentaminoRightR4C2 = findViewById( R.id.ivPentaminoRightR4C2) ;
        m_ivPentaminoLeftR4C3 = findViewById( R.id.ivPentaminoLeftR4C3) ; m_ivPentaminoRightR4C3 = findViewById( R.id.ivPentaminoRightR4C3) ;
        m_ivPentaminoLeftR5C1 = findViewById( R.id.ivPentaminoLeftR5C1) ; m_ivPentaminoRightR5C1 = findViewById( R.id.ivPentaminoRightR5C1) ;
        m_ivPentaminoLeftR5C2 = findViewById( R.id.ivPentaminoLeftR5C2) ; m_ivPentaminoRightR5C2 = findViewById( R.id.ivPentaminoRightR5C2) ;
        m_ivPentaminoLeftR5C3 = findViewById( R.id.ivPentaminoLeftR5C3) ; m_ivPentaminoRightR5C3 = findViewById( R.id.ivPentaminoRightR5C3) ;

        mPentaminoLeft = mPentaminoNoShape ;
        displayShapeLeft();
        mPentaminoRight = mPentaminoNoShape ;
        displayShapeRight();

        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        // Set up the user interaction to manually show or hide the system UI.
        mBtnThrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int r = 0 ;

                boolean bThrow = true ;
                while( bThrow ) {
                    // Throw the dice
                    r = new Random().nextInt(71);  // [0...5]
                    r = r % 6 + 1;

                    if( iDiceValue == r ) {
                        iCountWithSameDiceValue ++ ;
                    }

                    if( MAX_THROW_WITH_SAME_VALUE >= iCountWithSameDiceValue ) {
                        iCountWithSameDiceValue = 0 ;
                        bThrow = false ;
                    }
                }

                iDiceValue = r ;
                mTvDice.setText(String.valueOf(iDiceValue));

                switch(r) {
                    case 1:
                        mPentaminoLeft = mPentamino1Left ;
                        mPentaminoRight = mPentamino1Right ;
                        break;
                    case 2:
                        mPentaminoLeft = mPentamino2Left ;
                        mPentaminoRight = mPentamino2Right ;
                        break;
                    case 3:
                        mPentaminoLeft = mPentamino3Left ;
                        mPentaminoRight = mPentamino3Right ;
                        break;
                    case 4:
                        mPentaminoLeft = mPentamino4Left ;
                        mPentaminoRight = mPentamino4Right ;
                        break;
                    case 5:
                        mPentaminoLeft = mPentamino5Left ;
                        mPentaminoRight = mPentamino5Right ;
                        break;
                    case 6:
                        mPentaminoLeft = mPentamino6Left ;
                        mPentaminoRight = mPentamino6Right ;
                        break;
                    default:
                        mPentaminoLeft = mPentaminoNoShape ;
                        mPentaminoRight = mPentaminoNoShape ;
                        break;

                }
                displayShapeLeft();
                displayShapeRight();
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        //findViewById(R.id.dummy_button).setOnTouchListener(mDelayHideTouchListener);
    }

    private int getShapeColorLeft(int r) {
        switch(r){
            case 1 :
                return getResources().getColor(R.color.pentoShape1L) ;
            case 2 :
                return getResources().getColor(R.color.pentoShape2L) ;
            case 3 :
                return getResources().getColor(R.color.pentoShape3L) ;
            case 4 :
                return getResources().getColor(R.color.pentoShape4L) ;
            case 5 :
                return getResources().getColor(R.color.pentoShape5L) ;
            case 6 :
                return getResources().getColor(R.color.pentoShape6L) ;
            default :
                return getResources().getColor(R.color.pentoGray) ;
        }
    }

    private int getShapeColorRight(int r) {
        switch(r){
            case 1 :
                return getResources().getColor(R.color.pentoShape1R) ;
            case 2 :
                return getResources().getColor(R.color.pentoShape2R) ;
            case 3 :
                return getResources().getColor(R.color.pentoShape3R) ;
            case 4 :
                return getResources().getColor(R.color.pentoShape4R) ;
            case 5 :
                return getResources().getColor(R.color.pentoShape5R) ;
            case 6 :
                return getResources().getColor(R.color.pentoShape6R) ;
            default :
                return getResources().getColor(R.color.pentoGray) ;
        }
    }

    private void displayShapeLeft(){
        m_ivPentaminoLeftR1C1.setVisibility( (0==mPentaminoLeft[0]?View.INVISIBLE:View.VISIBLE) );
        m_ivPentaminoLeftR1C1.setBackgroundColor(getShapeColorLeft(mPentaminoLeft[0]));
        m_ivPentaminoLeftR1C2.setVisibility((0==mPentaminoLeft[1]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoLeftR1C2.setBackgroundColor(getShapeColorLeft(mPentaminoLeft[1]));
        m_ivPentaminoLeftR1C3.setVisibility((0==mPentaminoLeft[2]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoLeftR1C3.setBackgroundColor(getShapeColorLeft(mPentaminoLeft[2]));

        m_ivPentaminoLeftR2C1.setVisibility((0==mPentaminoLeft[3]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoLeftR2C1.setBackgroundColor(getShapeColorLeft(mPentaminoLeft[3]));
        m_ivPentaminoLeftR2C2.setVisibility((0==mPentaminoLeft[4]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoLeftR2C2.setBackgroundColor(getShapeColorLeft(mPentaminoLeft[4]));
        m_ivPentaminoLeftR2C3.setVisibility((0==mPentaminoLeft[5]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoLeftR2C3.setBackgroundColor(getShapeColorLeft(mPentaminoLeft[5]));

        m_ivPentaminoLeftR3C1.setVisibility((0==mPentaminoLeft[6]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoLeftR3C1.setBackgroundColor(getShapeColorLeft(mPentaminoLeft[6]));
        m_ivPentaminoLeftR3C2.setVisibility((0==mPentaminoLeft[7]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoLeftR3C2.setBackgroundColor(getShapeColorLeft(mPentaminoLeft[7]));
        m_ivPentaminoLeftR3C3.setVisibility((0==mPentaminoLeft[8]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoLeftR3C3.setBackgroundColor(getShapeColorLeft(mPentaminoLeft[8]));

        m_ivPentaminoLeftR4C1.setVisibility((0==mPentaminoLeft[9]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoLeftR4C1.setBackgroundColor(getShapeColorLeft(mPentaminoLeft[9]));
        m_ivPentaminoLeftR4C2.setVisibility((0==mPentaminoLeft[10]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoLeftR4C2.setBackgroundColor(getShapeColorLeft(mPentaminoLeft[10]));
        m_ivPentaminoLeftR4C3.setVisibility((0==mPentaminoLeft[11]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoLeftR4C3.setBackgroundColor(getShapeColorLeft(mPentaminoLeft[11]));

        m_ivPentaminoLeftR5C1.setVisibility((0==mPentaminoLeft[12]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoLeftR5C1.setBackgroundColor(getShapeColorLeft(mPentaminoLeft[12]));
        m_ivPentaminoLeftR5C2.setVisibility((0==mPentaminoLeft[13]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoLeftR5C2.setBackgroundColor(getShapeColorLeft(mPentaminoLeft[13]));
        m_ivPentaminoLeftR5C3.setVisibility((0==mPentaminoLeft[14]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoLeftR5C3.setBackgroundColor(getShapeColorLeft(mPentaminoLeft[14]));
    }

    private void displayShapeRight(){
        m_ivPentaminoRightR1C1.setVisibility( (0==mPentaminoRight[0]?View.INVISIBLE:View.VISIBLE) );
        m_ivPentaminoRightR1C1.setBackgroundColor(getShapeColorRight(mPentaminoRight[0]));
        m_ivPentaminoRightR1C2.setVisibility((0==mPentaminoRight[1]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoRightR1C2.setBackgroundColor(getShapeColorRight(mPentaminoRight[1]));
        m_ivPentaminoRightR1C3.setVisibility((0==mPentaminoRight[2]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoRightR1C3.setBackgroundColor(getShapeColorRight(mPentaminoRight[2]));

        m_ivPentaminoRightR2C1.setVisibility((0==mPentaminoRight[3]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoRightR2C1.setBackgroundColor(getShapeColorRight(mPentaminoRight[3]));
        m_ivPentaminoRightR2C2.setVisibility((0==mPentaminoRight[4]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoRightR2C2.setBackgroundColor(getShapeColorRight(mPentaminoRight[4]));
        m_ivPentaminoRightR2C3.setVisibility((0==mPentaminoRight[5]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoRightR2C3.setBackgroundColor(getShapeColorRight(mPentaminoRight[5]));

        m_ivPentaminoRightR3C1.setVisibility((0==mPentaminoRight[6]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoRightR3C1.setBackgroundColor(getShapeColorRight(mPentaminoRight[6]));
        m_ivPentaminoRightR3C2.setVisibility((0==mPentaminoRight[7]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoRightR3C2.setBackgroundColor(getShapeColorRight(mPentaminoRight[7]));
        m_ivPentaminoRightR3C3.setVisibility((0==mPentaminoRight[8]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoRightR3C3.setBackgroundColor(getShapeColorRight(mPentaminoRight[8]));

        m_ivPentaminoRightR4C1.setVisibility((0==mPentaminoRight[9]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoRightR4C1.setBackgroundColor(getShapeColorRight(mPentaminoRight[9]));
        m_ivPentaminoRightR4C2.setVisibility((0==mPentaminoRight[10]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoRightR4C2.setBackgroundColor(getShapeColorRight(mPentaminoRight[10]));
        m_ivPentaminoRightR4C3.setVisibility((0==mPentaminoRight[11]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoRightR4C3.setBackgroundColor(getShapeColorRight(mPentaminoRight[11]));

        m_ivPentaminoRightR5C1.setVisibility((0==mPentaminoRight[12]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoRightR5C1.setBackgroundColor(getShapeColorRight(mPentaminoRight[12]));
        m_ivPentaminoRightR5C2.setVisibility((0==mPentaminoRight[13]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoRightR5C2.setBackgroundColor(getShapeColorRight(mPentaminoRight[13]));
        m_ivPentaminoRightR5C3.setVisibility((0==mPentaminoRight[14]?View.INVISIBLE:View.VISIBLE));
        m_ivPentaminoRightR5C3.setBackgroundColor(getShapeColorRight(mPentaminoRight[14]));
    }

//    private void display_Shape_1_Left(){
//        m_ivPentaminoLeftR1C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR1C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR1C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR1C2.setBackgroundColor(getResources().getColor(R.color.pentoShape1L));
//        m_ivPentaminoLeftR1C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR1C3.setBackgroundColor(getResources().getColor(R.color.pentoShape1L));
//
//        m_ivPentaminoLeftR2C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR2C1.setBackgroundColor(getResources().getColor(R.color.pentoShape1L));
//        m_ivPentaminoLeftR2C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR2C2.setBackgroundColor(getResources().getColor(R.color.pentoShape1L));
//        m_ivPentaminoLeftR2C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR2C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR3C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR3C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR3C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR3C2.setBackgroundColor(getResources().getColor(R.color.pentoShape1L));
//        m_ivPentaminoLeftR3C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR3C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR4C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR4C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR4C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR4C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR4C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR4C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR5C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR5C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR5C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR5C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR5C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR5C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//    }
//
//    private void display_Shape_1_Right(){
//        m_ivPentaminoRightR1C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR1C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR1C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR1C2.setBackgroundColor(getResources().getColor(R.color.pentoShape1R));
//        m_ivPentaminoRightR1C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR1C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR2C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR2C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR2C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR2C2.setBackgroundColor(getResources().getColor(R.color.pentoShape1R));
//        m_ivPentaminoRightR2C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR2C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR3C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR3C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR3C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR3C2.setBackgroundColor(getResources().getColor(R.color.pentoShape1R));
//        m_ivPentaminoRightR3C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR3C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR4C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR4C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR4C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR4C2.setBackgroundColor(getResources().getColor(R.color.pentoShape1R));
//        m_ivPentaminoRightR4C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR4C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR5C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR5C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR5C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR5C2.setBackgroundColor(getResources().getColor(R.color.pentoShape1R));
//        m_ivPentaminoRightR5C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR5C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//    }
//
//    private void display_Shape_2_Left(){
//        m_ivPentaminoLeftR1C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR1C1.setBackgroundColor(getResources().getColor(R.color.pentoShape2L));
//        m_ivPentaminoLeftR1C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR1C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR1C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR1C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR2C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR2C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR2C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR2C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR2C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR2C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR3C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR3C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR3C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR3C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR3C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR3C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR4C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR4C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR4C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR4C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR4C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR4C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR5C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR5C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR5C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR5C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR5C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR5C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//    }
//
//    private void display_Shape_2_Right(){
//        m_ivPentaminoRightR1C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR1C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR1C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR1C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR1C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR1C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR2C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR2C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR2C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR2C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR2C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR2C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR3C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR3C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR3C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR3C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR3C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR3C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR4C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR4C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR4C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR4C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR4C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR4C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR5C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR5C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR5C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR5C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR5C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR5C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//    }
//
//    private void display_Shape_3_Left(){
//        m_ivPentaminoLeftR1C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR1C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR1C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR1C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR1C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR1C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR2C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR2C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR2C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR2C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR2C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR2C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR3C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR3C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR3C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR3C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR3C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR3C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR4C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR4C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR4C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR4C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR4C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR4C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR5C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR5C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR5C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR5C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR5C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR5C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//    }
//
//    private void display_Shape_3_Right(){
//        m_ivPentaminoRightR1C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR1C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR1C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR1C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR1C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR1C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR2C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR2C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR2C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR2C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR2C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR2C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR3C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR3C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR3C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR3C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR3C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR3C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR4C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR4C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR4C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR4C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR4C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR4C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR5C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR5C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR5C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR5C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR5C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR5C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//    }
//
//    private void display_Shape_4_Left(){
//        m_ivPentaminoLeftR1C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR1C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR1C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR1C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR1C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR1C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR2C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR2C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR2C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR2C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR2C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR2C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR3C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR3C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR3C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR3C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR3C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR3C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR4C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR4C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR4C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR4C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR4C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR4C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR5C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR5C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR5C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR5C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR5C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR5C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//    }
//
//    private void display_Shape_4_Right(){
//        m_ivPentaminoRightR1C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR1C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR1C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR1C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR1C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR1C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR2C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR2C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR2C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR2C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR2C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR2C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR3C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR3C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR3C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR3C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR3C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR3C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR4C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR4C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR4C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR4C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR4C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR4C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR5C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR5C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR5C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR5C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR5C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR5C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//    }
//
//    private void display_Shape_5_Left(){
//        m_ivPentaminoLeftR1C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR1C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR1C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR1C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR1C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR1C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR2C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR2C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR2C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR2C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR2C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR2C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR3C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR3C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR3C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR3C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR3C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR3C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR4C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR4C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR4C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR4C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR4C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR4C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR5C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR5C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR5C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR5C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR5C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR5C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//    }
//
//    private void display_Shape_5_Right(){
//        m_ivPentaminoRightR1C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR1C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR1C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR1C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR1C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR1C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR2C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR2C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR2C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR2C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR2C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR2C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR3C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR3C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR3C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR3C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR3C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR3C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR4C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR4C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR4C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR4C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR4C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR4C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR5C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR5C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR5C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR5C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR5C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR5C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//    }
//
//    private void display_Shape_6_Left(){
//        m_ivPentaminoLeftR1C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR1C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR1C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR1C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR1C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR1C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR2C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR2C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR2C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR2C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR2C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR2C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR3C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR3C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR3C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR3C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR3C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR3C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR4C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR4C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR4C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR4C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR4C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR4C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR5C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR5C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR5C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR5C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR5C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoLeftR5C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//    }
//
//    private void display_Shape_6_Right(){
//        m_ivPentaminoRightR1C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR1C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR1C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR1C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR1C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR1C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR2C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR2C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR2C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR2C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR2C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR2C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR3C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR3C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR3C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR3C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR3C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR3C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR4C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR4C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR4C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR4C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR4C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR4C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR5C1.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR5C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR5C2.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR5C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR5C3.setVisibility(View.INVISIBLE);
//        m_ivPentaminoRightR5C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//    }
//
//    private void display_NoShape_Left(){
//        m_ivPentaminoLeftR1C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR1C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR1C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR1C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR1C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR1C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR2C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR2C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR2C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR2C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR2C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR2C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR3C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR3C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR3C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR3C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR3C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR3C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR4C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR4C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR4C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR4C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR4C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR4C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoLeftR5C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR5C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR5C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR5C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoLeftR5C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoLeftR5C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//    }
//
//    private void display_NoShape_Right(){
//        m_ivPentaminoRightR1C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR1C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR1C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR1C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR1C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR1C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR2C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR2C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR2C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR2C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR2C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR2C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR3C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR3C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR3C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR3C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR3C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR3C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR4C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR4C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR4C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR4C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR4C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR4C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//
//        m_ivPentaminoRightR5C1.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR5C1.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR5C2.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR5C2.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//        m_ivPentaminoRightR5C3.setVisibility(View.VISIBLE);
//        m_ivPentaminoRightR5C3.setBackgroundColor(getResources().getColor(R.color.pentoGray));
//    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        //mControlsView.setVisibility(View.GONE);
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in delay milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}